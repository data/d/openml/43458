# OpenML dataset: data-science-survey-on-Kaggle

https://www.openml.org/d/43458

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Throughout the world of data science, there are many languages and tools that can be used to complete a given task. While you are often able to use whichever tool you prefer, it is often important for analysts to work with similar platforms so that they can share their code with one another. Learning what professionals in the data science industry use while at work can help you gain a better understanding of things that you may be asked to do in the future.

Content
In this project, we are going to find out what tools and languages professionals use in their day-to-day work. Our data comes from the Kaggle Data Science Survey which includes responses from over 10,000 people that write code to analyze data in their daily work.

Acknowledgements
Kaggle and DataCamp helped me with the dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43458) of an [OpenML dataset](https://www.openml.org/d/43458). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43458/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43458/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43458/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

